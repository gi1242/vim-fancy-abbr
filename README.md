# fancy-abbr: Fancy Abbreviations for NeoVim/Vim (in Lua)

The fancy abbreviations allow you to:

1. Define abbreviations for things that Vim doesn't consider an abbreviation.
   For instance `\li`, or `\a`

2. Will expand even after you `<Ctrl-W>` out. For instance with

        :iab eq equation

   If you type `equa<ctrl-w>eq<space>` Vim/NeoVim don't expand it. This plugin
   does.

3. Simpler configuration interface 😄

4. Few other advanced options.

See the `fancy-abbr/tex.lua` file for configuration options and examples.

## Installation / Configuration

If you use [lazy.nvim](https://github.com/folke/lazy.nvim) then install and
configure this plugin using this in your plugin spec:

    { 'https://codeberg.org/gi1242/vim-fancy-abbr.git',
      dev=true,
      opts={
        runtime_abbr_defs={
          'fancy-abbr/tex.lua',
          'fancy-abbr/emoji.lua',
          'fancy-abbr/accents.lua',
          'opt/fancy-defs.lua', -- create this file for your custom abbrs
        }
      }
    }

Alternately, clone the repository in your plugin path, and configure it with

    require('fancy-abbr').setup({...})

Options are merged with current options. So to override an option you have to
explicitly specify the new value.


## Options

* **method:** Either `maps` (recommended) or `autocmd` (slower).

    Using
  `autocmd` means expansions are checked on every character press, which
  could be expensive if you have a lot of abbreviations. If using `maps` then
  expansions are only checked on whitespace or punctuation. This should be
  enough for most setups. If all your abbreviations end on whitespace /
  punctuation, and just a few other special characters use `maps`.

    **Note:**
  When using maps, normal abbreviations won't work. Convert them to fancy
  abbreviations by replacing `:iab foo fooExpanded` with `:Fab foo
  fooExpanded`.

* **runtime_abbr_defs:** Runtime files containing abbreviations. Each of these
  files are sourced using `runtime` before initializing this plugin. Abbreviations from the following files are loaded by default:

        fancy-abbr/tex.lua
        fancy-abbr/emoji.lua
        fancy-abbr/accents.lua

    Write
  your own abbreviations in a separate file and add it at the end of this list
  (or replace this list with it).

* **debug:** Enabling profiling / timing info.

## Adding abbreviations

Use the lua functions `add` and `add_abbr` to add your own abbreviations. They
are documented in `lua/fancy-abbr.lua`, and there are examples in
`fancy-abbr/tex.lua`.

## Performance

So, if you have a large (few thousand) number of abbreviations you may want to
check the performance of this plugin. Enable profiling with:

    require('fancy-abbr').setup({debug=true})

Type a bit, and then run

    :lua require('fancy-abbr').print_stats()

to see some statistics about run times.

This plugin dynamically caches abbreviations matching the last two characters.
So the second time a character is typed, the plugin matches abbreviations much
faster. To reset the statistics (without clearing the cache) do

    :lua require('fancy-abbr').runtimes = {}

With my setup (1400+ abbreviations, Intel i5-1135G7 2.4ghz), it takes about
1.6ms per character before the cache fills up and then 0.07ms after the cache
fills up.

## Helper commands / mappings

* `FancyAbbrEnable`, `FancyAbbrDisable`: Enable / disable the plugin.
* `<Plug>FancyAbbrEnable`, `<Plug>FancyAbbrDisable`: Mappings to enable / disable the plugin.
* `FancyAbbrPrintStats`: Show stats, (and timing info if debug is enabled)
* `FancyAbbrResetStats`: Reset stats
