-- Fancy imaps for latex

if vim.b.did_ftplugin_imaps then
  return
end
vim.b.did_ftplugin_imaps = 1

local fi = require('fancy-imaps')
if vim.list_contains( fi.processed_modules, 'latex' ) then
  fi.init()
  return
end
table.insert( fi.processed_modules, 'latex' )

fi.add({
  defaults={ ft={'tex', 'markdown'} },

  { '*', '[^%a]%a', '^*', cn='markup.math' }, -- x* → x^*

  -- Auto braces around long scripts
  { '+-(', { '[_^]', '{%k', ra='}'} },
  { '+-=', { '[_^]%w', '<Left>{<Right>%k', ra='}' } },
  { '0123456789', {
    { '[_^][%d.]', '<Left>{<Right>%k', ra='}' },

    -- Replace x1 etc with x_1 in math mode
    { '[%s$]%a', '_%k', cn='markup.math' },
  }},

  -- Add matching '['
  { '[', { '[%a*}%]]', '[', ra=']' } },
  { ']', { '[^\\]', '<Right>', ma='%]' } },

  -- Add matching '{'
  { '{', {
    { '[^\\]', '{', ra='}' },
    { '^', '{', ra='}' },
  }},
  { '}', {
    { mb='[^\\]', ma='}', rb='<Right>' },

    -- Add close tag on when '}' is pressed after \begin{env}
    { mb=[[\begin{(@?%w+%*?)]], rb=[[}<CR>\end{%1}<c-d><Up><End>]] },
    { mb=[[\begin{(@?%w+%*?)]], ma='}', rb=[[}<CR>\end{%1<Up><End>]] },
  }},

  { '$', {
      -- Add matching '$'
      { '^', '$', ra='$', cn='!markup.math' },
      { '[^$]', '$', ra='$', cn='!markup.math' },
      { ma='%$', rb='<Right>' },

      -- Replace $$ with \begin{equation} .. \end{equation}
      { '%$', [[<BS>\begin{equation}<CR>\end{equation}<C-d><Up><End>]],
	  ma='$' },
      { mb='%$', ma='%$', bs=1,
	  rb=[[<Del>\begin{equation}<CR>\end{equation}<C-d><Up><End>]] },
      { mb='%~%$', ma='%$', bs=2,
	  rb=[[ <Del>\begin{equation}<CR>\end{equation}<C-d><Up><End>]] },
  }},

  -- Pressing <BS> deletes the extra stuff we added.
  { {'<BS>'}, {
    { mb='[^\\]{', ma='}', rb='<BS><Del>' },
    { mb='^{', ma='}', rb='<BS><Del>' },
    { mb='[^\\]%[', ma='%]', rb='<BS><Del>' },
    { mb='^%[', ma='%]', rb='<BS><Del>' },
    { mb='%$', ma='%$', rb='<BS><Del>' },
    { mb='~%$', ma='%$', rb='<BS><BS><Del> ' },
  }},
})

fi.add({
  defaults = { ft='tex' },

  {'$', {
    { '%w%s', '<BS>~$', ra='$', cn='!markup.math' }, -- add ~ when entering math mode
  }},

  {'"', {
    { '%s', '``' },
    { '[%w%p]', "''" },
  }},
  { '{', {
    -- replace \ci{ with ~\cite{
    { mb='\\ci', rb='te{', ra='}' },
    { mb='%S%s\\ci', bs=4, rb='~\\cite{', ra='}' },
    { mb='%S%s\\cite', bs=6, rb='~\\cite{', ra='}' },

    { mb='\\re', rb='f{', ra='}' },
    { mb='%S%s\\re', bs=4, rb='~\\ref{', ra='}' },
    { mb='%S%s\\ref', bs=5, rb='~\\ref{', ra='}' },

    { mb='\\eq', rb='ref{e:', ra='}' },
    { mb='%s\\eq', bs=4, rb='~\\eqref{e:', ra='}' },
    { mb='%s\\eqref', bs=7, rb='~\\eqref{e:', ra='}' },
  }},
})

fi.add({
  defaults = { ft='markdown' },

  { '{', {
    { mb='\\eq', rb='ref{e:', ra='}' },
    { mb='\\re', rb='f{', ra='}' },
  }},
  { '0123456789', " '", '<BS><BS> ’%k' },
})

fi.init()
