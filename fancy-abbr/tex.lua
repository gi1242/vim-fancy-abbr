local fa = require('fancy-abbr')

-- Tex abbreviations
local ft={ 'tex', 'text', 'mail', 'markdown', 'mmd' }
fa.tex = {}
fa.tex.ft = ft

--[[ Punctuation ranges:
    !-/: !"#$%&'()*+,-./
    0-9: 0123456789
    :-@: :;<=>?@
    [-`: [\]^_`
    {-~: {|}~
--]]

local sp_no_paren='[%s!"$-\')-/:-?[-`{-~]' -- Punctuation, Exclude @#(
local sp='[%s!"$-/:-?[-`{-~]' -- Punctuation, Exclude @#
local sp_no_star='[%s!"$-)+-/:-?[-`{-~]' -- Punctuation, Exclude @#
fa.tex.sp = sp
fa.tex.sp_no_paren = sp_no_paren


--{{{1 Commands
local commands = {p='\\', ft=ft, t=sp}
fa.tex.commands = commands

fa.add_abbrs( commands, {
  sec='section',
  ss='subsection',
  sss='subsubsection',
  para='paragraph',
  spar='subparagraph',
  bg='begingroup',
  eg='endgroup',
  mb='mathbb',
  mbb='mathbb',
  mbf='mathbf',
  mc='mathcal',
  ms='mathscr',
  mf='mathfrak',
  { a='li', r='linewidth', t='%s' },
  { a='li', r='lim', t='_' },
  { a='in', r='includegraphics', t='[{[]' },
  { a='in', r='int', t='_' },
  di='displaystyle',
  tb='textbf',
  em='emph',
  on='operatorname',
  te='text',
  tt='texttt',
  tit='textit',
  mi='mathit',
  mrm='mathrm',
  it='item',
  qd='quad',
  qq='qquad',
  qa=[[quad\text{and}\quad]],
  qqa=[[qquad\text{and}\qquad]],
  hr=[[bigskip\hrule\bigskip]],
  hl='hline',
})

fa.add_abbrs( commands, {
  be={ t='{', r='begin',  },
  beg='begin',
  --en='<Backspace><Plug>(vimtex-env-close)<Backspace>',
  -- Close most recent environment on \en
  en=function()
    local pos= vim.fn.searchpairpos( [[\v\\begin\{[@a-zA-Z]+\*?\}]], '',
	[[\v\\end\{[@a-zA-Z]+\*?\}]], 'bn' )
    local lnum, cnum = pos[1]-1, pos[2]
    if lnum == -1 then
      return 'end{'
    else
      return '<C-d>end{' ..
	vim.api.nvim_buf_get_lines( 0, lnum, lnum+1, true )[1]
	  :sub( cnum ):match( [[\begin{([@%a]+*?})]] )
    end
  end,
})

fa.add_abbrs( commands, {
  lab='label',
  la={ r='label', t='{' },
  nn='nonumber',
  xr='xrightarrow',
  xl='xleftarrow',
})

fa.add_abbrs( commands, {
  ov='overline',
  ol='overline',
  un='underline',
  ul='underline',
  ob='overbrace',
  ub='underbrace',
})

fa.add_abbrs( commands, {
  su='sum',
  pr='prod',
  sb='subseteq',
  subs='subseteq',
  sp='supseteq',
  sups='supseteq',
  bcu='bigcup',
  bcup='bigcup',
  bca='bigcap',
  bcap='bigcap',
  cu='cup',
  ca='cap',
  es='emptyset',
  co='colon',
  sm='setminus',
  we='wedge',
  vn='varnothing',

  fr='frac',
  sq='sqrt',
  tsub='textsubscript',
  tsup='textsuperscript',
  tp='texorpdfstring',

  pb='parbox',
  rb='raisebox',

  qu='question',
  -- pa='part',

  oo='infty',
  -- st='star',
  cd='cdot',
})

--{{{1 References
--[[ fa.add_abbrs( commands, {
  eq={ r='eqref{e:', tr='' },
  re={ r='ref{', tr='' },
}) --]]

local refs={ft=ft, p={'^', '[%s%(]'}, t='~', tr=''}
fa.tex.refs = refs

local function uc1(s)
  return s:sub(1,1):upper()..s:sub(2)
end

for k, v in pairs({
  ap={s='appendix', p='appendices', r='\\ref{s:' },
  co={s='corollary', p='corollaries', r='\\ref{c:' },
  de={s='definition', p='definitions', r='\\ref{d:' },
  eq={s='equation', p='equations', r='\\eqref{e:' },
  fi={s='figure', p='figures', r='\\ref{f:' },
  ie={s='inequality', p='inequalities', r='\\eqref{e:' },
  le={s='lemma', p='lemmas', r='\\ref{l:' },
  pb={s='problem', p='problems', r='\\ref{p:' },
  cj={s='conjecture', p='conjectures', r='\\ref{C:' },
  pr={s='proposition', p='propositions', r='\\ref{p:' },
  re={s='remark', p='remarks', r='\\ref{r:' },
  se={s='section', p='sections', r='\\ref{s:' },
  th={s='theorem', p='theorems', r='\\ref{t:' },
  ass={s='assumption', p='assumptions', r='\\ref{a:' },
}) do
  local r = v.r .. '}<Left>'
  fa.add_abbrs( refs, {
    { a=k, r=v.s..'~'..r },
    { a=k..'s', r=v.p..'~'..r },
    { a=uc1(k), r=uc1(v.s)..'~'..r },
    { a=uc1(k)..'s', r=uc1(v.p)..'~'..r },
  })
end

--{{{1 Fractions
for n=1, 9 do
  fa.add( commands, {a='fr'..n, r='frac{'..n..'}{', tr='', t='[%s{]' } )
  fa.add( commands, {a='fr'..n, t='[1-9]', r='frac{'..n..'}{', tr='%t}' } )
end


-- {{{1 Colors
fa.add_abbrs( commands, {
  tc='textcolor',
  { a='co', r='color', t='{' }
})

for a, c in pairs({ r='red', g='green', b='blue', o='orange', k='black' }) do
  fa.add_abbrs( commands, {
    { a='co'..a, r='color{'..c..'}' },
    { a='tc'..a, r='textcolor{'..c..'}' },
  })
end

-- {{{1 Accents
fa.add_abbrs( commands, {
  ti='tilde',
  wt='widetilde',
  ba='bar',
  ha='hat',
  wh='widehat',
  {a='do', r='dot'},
  dd='ddot',
  mr='mathring',
  ck='check',
})

-- {{{1 Derivatives / integrals
fa.add_abbrs( commands, {
  na='nabla',
  pa={r='partial', t='[_^%s\\]' }
})

for c in ('jkrstxyz0123'):gmatch('.') do
  fa.add_abbrs( commands, {
    {a='p'..c, r='partial_'..c},
    {a='p'..c..2, r='partial_'..c..'^2'},
    {a='p'..c..3, r='partial_'..c..'^3'},
  })
end

for c in ('ih'):gmatch('.') do
  fa.add_abbrs( commands, {
    {a='pa'..c, r='partial_'..c},
    {a='pa'..c..2, r='partial_'..c..'^2'},
    {a='pa'..c..3, r='partial_'..c..'^3'},
  })
end

fa.add_abbrs( commands, {
  ir=[[int_\R]],
  ir2=[[int_{\R^2}]],
  ir3=[[int_{\R^3}]],
  ird=[[int_{\R^d}]],
  irn=[[int_{\R^n}]],
  irp=[[int_{\R^+}]],
  iz1=[[int_0^1]],
  izt=[[int_0^t]],
  izT=[[int_0^T]],
  izi=[[int_0^\infty]],
  imii=[[int_{-\infty}^\infty]],
  { a='ir', r=[[int_{\R]], t='%^' },
  { a='iz', r=[[int_0^]], t='%s%^'},
})

--{{{1 Greek letters
for a, r in pairs({
  al='alpha',
  be='beta',
  ga='gamma',
  de='delta',
  ep='epsilon',
  ve='varepsilon',
  ze='zeta',
  et='eta',
  th='theta',
  vt='vartheta',
  io='iota',
  ka='kappa',
  la='lambda',
  mu='mu',
  nu='nu',
  xi='xi',
  -- o='o'
  pi='pi',
  vpi='varpi',
  rh='rho',
  vr='varrho',
  si='sigma',
  vs='varsigma',
  ta='tau',
  up='upsilon',
  ph='phi',
  vp='varphi',
  ch='chi',
  ps='psi',
  om='omega',

  Ga='Gamma',
  vG='varGamma',
  De='Delta',
  vD='varDelta',
  Th='Theta',
  vT='varTheta',
  La='Lambda',
  vL='varLambda',
  Xi='Xi',
  vX='varXi',
  Pi='Pi',
  vPi='varPi',
  Si='Sigma',
  vS='varSigma',
  Up='Upsilon',
  vU='varUpsilon',
  Ph='Phi',
  vP='varPhi',
  vPh='varPhi',
  Ps='Psi',
  vPs='varPsi',
  Om='Omega',
  vO='varOmega',
}) do
  fa.add_abbrs( commands, {
    {a=a, t=sp_no_star, r=r},
    {a=a, t='[01]', r=r..'_'},
    {a=a, t='[*2-9]', r=r..'^'},
    -- * superscript doesn't seem to work.
  })
end

-- {{{1 Sub / super scripts
local scripts = { ft=ft, p='[_^]', t=sp }
fa.tex.scripts = scripts

local subscripts = { ft=ft, p='_', t=sp }
fa.tex.subscripts = subscripts

local vars = { ft=ft, p={'^', '[%s${}()[%]=<>%-+~/]'}, t=sp }
fa.tex.vars = vars

function fa.tex.var_ab( a, r )
  fa.add( scripts, { a=a, r='{'..r..'}', t=sp_no_paren } )
  fa.add( scripts, { a=a, r='{'..r, t='%(' } )
  fa.add( vars, { a=a, r=r } )
end


--[[ 2024-07-29 Move this to imaps instead
fa.add_abbrs( scripts, {
  m1='{-1}',
})

for c in ('ijkmnMN'):gmatch('.') do
  fa.add_abbrs( scripts, {
    { a=c..'p1', r='{'..c..'+1}' },
    { a=c..'m1', r='{'..c..'-1}' },
    -- { a=c..'+1', r='{'..c..'+1}' },
    -- { a=c..'-1', r='{'..c..'-1}' },
  })
end

-- replace _x=1  with _{x=1}, etc.
for c in ('123'):gmatch('.') do
  fa.add_abbrs( scripts, {
    { a=c, p='[_^]%a[+%-]', r=c..'}<Esc>hhhi{<Esc>%a' },
    { a=c, p='_%a=', r=c..'}<Esc>hhhi{<Esc>%a' },
    { a=c, p='_%a=%-', r=c..'}<Esc>hhhhi{<Esc>%a' },
    { a=c, p='[_^]%-', r=c..'}<Esc>hhi{<Esc>%a' },
  })
end
fa.add_abbrs( scripts, {
    { a='0', p='_%a=', r='0}<Esc>hhhi{<Esc>%a' },
})

for sa, sr in pairs({['0']=0, ['1']=1, ['-1']='{-1}', ['-i']='{-\\infty}',
    ['-p']='{-\\pi}' })
do
  for ea, er in pairs({[0]=0, [1]=1, ['i']='\\infty', ['p']='\\pi',
      n='n', N='N', M='M', d='d' })
  do
    fa.add( subscripts, {a=sa..ea, r=sr..'^'..er} )
    for v in ('xyzijkmnst'):gmatch('.') do
      fa.add( subscripts, {a=v..sa:gsub('^%-', 'm')..ea,
	r=('{%s=%s}^%s'):format(v, sr, er) } )
    end
  end
end

-- Variables x_1, x_i, etc
for v in ('txyzcN'):gmatch('.') do
  -- for s in ('0123456789ijk'):gmatch('.') do
  --   fa.add( scripts, { a=v..s, r='{'..v..'_'..s..'}' } )
  -- end
  for s in ('ijk'):gmatch('.') do
    --fa.add( vars, { a=v..s, r=v..'_'..s } )
    fa.tex.var_ab( v..s, v..'_'..s )
  end
  fa.add( vars, { a=v, t='[0-9]', r=v..'_' } )
  fa.add( scripts, { a=v, t='[0-9]', tr='%t}', r='{'..v..'_' } )
end
fa.add( vars, { a='S', t='[1-3]', r='S^' } ) -- S1, S2, ...
--]]

--{{{1 Spaces like L1, H1, Rd, etc

for s, e in pairs({
  [{ L='L', l='\\ell'}]
    = { p='p', q='q', r='r', [1]='1', [2]='2', oo='\\infty' },
  [{ R='\\R', C='\\C', T='\\T', Z='\\Z' }]
    = { d='d', m='m', n='n', M='M', N='N', [2]='2', [3]='3' },
  [{ H='H' }] =	{ s='s', m1='{-1}', [1]='1', [2]='2' },
  [{ C='C' }] =	{ k='k', al='\\alpha', be='\\beta',
      oo='\\infty', [1]='1', [2]='2', [11]='{1,1}', [12]='{1,2}' },
})
do
  for sa, sr in pairs(s) do
    for ea, er in pairs(e) do
      fa.tex.var_ab( sa..ea, sr..'^'..er )
    end
  end
end

-- {{{1 Phrases, etc.
-- ith jth, etc
local phrases = {ft=ft, p={'^', '[%s{([]'}, t=sp }
fa.tex.phrases = phrases

for c in ('ijknm'):gmatch('.') do
  fa.add( phrases, {a=c..'th', r='$'..c..'^\\text{th}$' } )
end

--[=[ 2024-07-30 Moved to tex_imaps
fa.add( phrases, {a='$$', t='\r',
  r=[[\begin{equation}<cr><C-d>\end{equation}<Esc>kA]]} )
fa.add( phrases, { p='^%s*', a='$', t='%$', tr='',
  r=[[\begin{equation}<cr><C-d>\end{equation}<Esc>kA]]} )
--]=]

-- }}}1

-- Reset buckets
fa.abbr_buckets = {}
