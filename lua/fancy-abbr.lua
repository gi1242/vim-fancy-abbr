local M = {}

M.opts = {
  method='maps', -- maps (recommended), autocmd
  debug=false,
  runtime_abbr_defs={
    'fancy-abbr/tex.lua',
    'fancy-abbr/emoji.lua',
    'fancy-abbr/accents.lua',
  }
}

local api = vim.api

M.disable = false
M.abbrs = {}	-- Table for abbreviations
M.abbr_buckets = {} -- Cached abbreviations by filetype (auto generated)
M.runtimes = {} -- Runtime of each call (for debugging)


local function set_default_table( val, default ) --{{{
  if type(val) == 'string' then return {val}
  else return val or default end
end --}}}
--[[
Add one abbreviation and sanitize defaults. Can be called in two forms:

  add(abbr, replacement)
  add( defaults, abbr )

In the first form both arguments are strings. This creates a new abbreviation
for `abbr` which expands to `replacement`.

In the second form `defaults` and `abbr` are both tables (abbr can be nil),
which are merged and a new abbreviation is added. Keys of the tables are:

  p : prefix (pattern, or table of patterns). Needs to be matched before
      abbreviation for it to be expanded.
  a : abbreviation string (required)
  t : trigger pattern. Needs to match the pressed character for abbreviation to
      be recorded.
  r : replacement string (required)
  tr : trigger character replacement (use %t for the char that triggered the
       expansion)
  ft : filetypes abbreviation is valid for

See the code below for defaults. Few examples:

  { a='NS', r='Navier--Stokes', ft='tex' },
  { p='\\', a='al', r='alpha' },
  { p='\\', a='eq', t='[%s{]', r='eqref{e:', tr='', ft='tex' },
  { p='%s', a='eq', t='~', r='equation~\\eqref{e:', tr='', ft='tex' },
  { a='Eq', t='~', r='Equation~eqref{e:', ft='tex' },
  { a='eq', r='equation' },
  { a='t1', r='t2' },
  { a='t2', r='t3' },

**NOTE:** For new abbreviations to take effect you will have to clear
hash buckets using M.abbr_buckets = {}.
--]]

function M.add( defaults, abbr ) --{{{
  if type(defaults) == 'string' and type(abbr) == 'string' then
    -- in M.add( abbreviation, replacement ) syntax
    M.add( {a=defaults, r=abbr} )
    return
  end

  local ab
  if abbr == nil then
    ab = defaults
  elseif abbr.a then
    ab = vim.tbl_extend( 'force', defaults, abbr )
  else
    -- In format { ab='replacement' }
    local a, r = next(abbr)
    if next( abbr, a ) then
      error( 'Badly defined abbreviation'..vim.inspect(abbr) )
    end
    ab = vim.tbl_extend( 'force', defaults, {a=a, r=r} )
  end

  -- Set defaults before adding abbreviation
  -- Default trigger is space or punctuation
  ab.t = ab.t or '[%s%p]'

  -- Default prefix matches white-space or line start.
  ab.p = set_default_table( ab.p, { '^', '%s' } )

  if type(ab.a) ~= 'string' or ( type(ab.r) ~= 'string' and type(ab.r) ~= 'function' )
  then
    error( 'Badly defined abbreviation: ' .. vim.inspect( ab ) )
  else
    -- if M.opts.debug then print( 'Inserting abbreviation', vim.inspect(ab) ) end
    table.insert( M.abbrs, ab )
  end
end --}}}


--[[
Helper function to add many abbreviations. E.g.

  add_abbrs( {ft='tex', p='\\'}, {
    al='alpha',
    eq={r='eqref{e:', tr='' },
    {a='in', r='int', t='_' },
  })

Note table keys have to be unique, and can't be reserved words.
--]]
function M.add_abbrs( defaults, abbrs ) --{{{
  for k, v in pairs( abbrs ) do
    M.add( defaults, vim.tbl_extend( 'force', {a=k},
      type(v)=='table' and v or {r=v} ) )
  end
end --}}}

function M.print_stats(print_buckets) --{{{
  if M.opts.debug then
    local time=0
    for _, t in ipairs( M.runtimes ) do
      time = time+t
    end
    print( ('Called %d times. Average runtime: %.3f ms')
      :format( #M.runtimes, 1000 * time / #M.runtimes ) )
  end

  local sizes = {}
  local max_size = 0
  local nbuckets = 0
  for k, v in pairs( M.abbr_buckets ) do
    nbuckets = nbuckets + 1
    sizes[k] = #v
    if #v > max_size then max_size = #v end
  end
  print( ('%d abbreviations, %d hash buckets with max size %d')
    :format( #M.abbrs, nbuckets, max_size ) )

  if print_buckets then print( vim.inspect(sizes) ) end
end --}}}

local function ft_matches( ft ) --{{{
  if ft == nil then return true
  elseif type(ft) == 'string' then
    if ft:sub(1,1) == '!' then
      return not vim.bo.ft:match(ft:sub(2))
    else
      return vim.bo.ft:match(ft)
    end
  else
    return vim.tbl_contains( ft, vim.bo.ft )
  end
end --}}}

local function fallback( char, old_map )
  if vim.tbl_isempty( old_map ) then
    return char
  elseif old_map.expr then
    return old_map.callback()
  else
    return old_map.rhs
  end
end

-- Main function to do the expansion
function M._expand( map_char, old_map ) --{{{
  --print( 'Inserted "'..vim.v.char..'"' )
  if M.disable then
    return fallback( map_char, old_map )
  end

  local pos = api.nvim_win_get_cursor(0)
  local cur_line = pos[1]-1
  local cur_col = pos[2]
  local line = api.nvim_buf_get_lines( 0, cur_line, cur_line+1, true )[1]
  local line_start = line:sub( 1, cur_col )
  --[[ print(('char="%s", trigger=%s, line=%d, col=%d, line_start="%s"')
      :format( vim.v.char, ab.t, cur_line, cur_col, line_start ) ) --]]

  local last_char = line_start:sub( -1, -1)
  local next_char
  if map_char then
    if map_char:lower() == '<esc>' then
      next_char = line:sub( cur_col+1, cur_col+1 )
      if next_char == '' then next_char = '\r' end
    else
      next_char = map_char
    end
  else
    next_char = vim.v.char
  end
  local hash_key = last_char .. next_char .. vim.bo.ft
  M.abbr_buckets[hash_key] = M.abbr_buckets[hash_key] or
    vim.tbl_filter( function(ab)
      return ( last_char ~= '' and vim.endswith(ab.a, last_char) )
	  and next_char:find(ab.t)
	  and ft_matches( ab.ft )
    end, M.abbrs )

  for _, ab in ipairs( M.abbr_buckets[hash_key] ) do
    -- ab.t will match next_char
    if vim.endswith( line_start, ab.a ) then
      local line_start = line_start:sub(1, -#ab.a-1 )
      for _, p in ipairs( ab.p ) do
	if line_start:find( p..'$' ) then
	  -- print( ( 'Matched %s' ):format( ab.a ) )
	  local c = map_char or vim.v.char
	  local tr = ab.tr and ab.tr:gsub('%%t', c) or c
	  local rep = type(ab.r) == 'string' and ab.r or ab.r()
	  if map_char then -- from mapping
	    return ('<BS>'):rep( #ab.a ) .. rep .. tr
	  else -- From autocommand
	    M.disable = true
	    vim.v.char = ''
	    local ks = api.nvim_replace_termcodes(
	      ('<BS>'):rep( #ab.a ) .. rep .. tr
		  .. '<Plug>FancyAbbrEnable;',
	      true, true, true)
	    api.nvim_feedkeys( ks, 'int', false )
	    return
	  end
	end
      end
      --[[ print( string.format( 'No match: line_start=%s, ab=%s',
	    line_start, ab.a ) ) --]]
    end
  end -- for _, ab
  -- Nothing matched.
  --[[ print( ('No match: map_char="%s", old_map="%s"')
    :format( map_char, old_map ) ) --]]
  return fallback(map_char, old_map)
end --}}}
M.expand = M._expand

-- Expand abbreviations when char is pressed (useful for <Esc>, <CR>)
function M.expand_on(char) --{{{
  local old_map = vim.fn.maparg( char, 'i', 0, 1 )
  if old_map.desc == 'FancyAbbr' then old_map = {} end
  vim.keymap.set( 'i', char,
    function() return M.expand(char, old_map) end,
    {expr=true, desc='FancyAbbr'} )
end --}}}

function M.time_expand( map_char, old_map ) --{{{
  local start=vim.fn.reltime()
  local ret = M._expand( map_char, old_map )
  table.insert( M.runtimes, vim.fn.reltimefloat( vim.fn.reltime(start) ) )
  return ret
end --}}}

function M.init() --{{{
  if M.opts.method == 'autocmd' then
    api.nvim_create_autocmd( 'InsertCharPre', {
      group = api.nvim_create_augroup( 'FancyAbbr', {} ),
      callback = M.expand,
    })
    M.expand_on('\n')
    M.expand_on('\r')
    M.expand_on('<Esc>')
  else
    for c in (' \n\r\t`~!@#$%^&*()-_=+{}[]|\\:;\'"<>,.?/1234567890')
	:gmatch('.')
    do
      M.expand_on(c)
    end
    M.expand_on('<Esc>')
    M.expand_on('<S-Space>')
  end
end --}}}

function M.setup( opts ) ---{{{
  M.opts = vim.tbl_extend( 'force', M.opts, opts or {} )

  M.expand = M.opts.debug and M.time_expand or M._expand

  -- Clear abbreviations
  M.abbrs = {}

  -- Load abbreviations
  for _, f in ipairs( M.opts.runtime_abbr_defs ) do
    vim.cmd( 'runtime ' .. f )
  end

  -- Call after abbreviations are defined
  M.init()

  -- Helper command to migrate existing Vim abbreviations. Replace `iab` with `Fab`
  vim.api.nvim_create_user_command( 'Fab',
    function(o) M.add( o.fargs[1], o.fargs[2] ) end,
    { nargs='+' }
  )

  -- Helpers to enable / disable plugin
  local all_modes = {'n', 'v', 'x', 's', 'o', 'i', 'l', 'c', 't' }
  vim.keymap.set( all_modes, '<Plug>FancyAbbrDisable;',
    function() M.disable=true end, {expr=true} )
  vim.keymap.set( all_modes, '<Plug>FancyAbbrEnable;',
    function() M.disable=false end, {expr=true} )
  vim.api.nvim_create_user_command( 'FancyAbbrDisable',
    function () M.disable=true end, {} )
  vim.api.nvim_create_user_command( 'FancyAbbrEnable',
    function () M.disable=false end, {} )

  vim.api.nvim_create_user_command( 'FancyAbbrReload',
    function ()
      package.loaded['fancy-abbr'] = nil
      require('fancy-abbr').setup(M.opts)
      print( 'Reloaded fancy-abbr' )
    end,
   {} )

  vim.api.nvim_create_user_command( 'FancyAbbrPrintStats',
    M.print_stats, {} )
  vim.api.nvim_create_user_command( 'FancyAbbrResetStats',
    function () M.runtimes = {} end, {} )
end ---}}}

return M
