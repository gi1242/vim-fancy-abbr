local M={}
M.imaps = {}
M.processed_modules = {}

local api = vim.api

local function fallback( char, old_map )
  if vim.tbl_isempty( old_map ) then
    return char
  elseif old_map.expr then
    return old_map.callback()
  else
    return old_map.rhs
  end
end

local function replace_tokens( str, tokens )
  return str:gsub( '(%%+)(%w)',
    function ( pct, tok )
      -- vim.print( ('pct=%s, tok=%s'):format( pct, tok ) )
      local tok_num = tonumber(tok)
      if tok_num ~= nil then
	-- print( ('tok=%s. tokens[%s]=%s, tokens='):format(
	--   tok, tok, tokens[tok]) )
	-- vim.print( tokens, type(tokens), tokens[1] )
	tok = tok_num
      end
      if tokens[tok] ~= nil then
	if #pct > 2 then
	  -- Doubled pct
	  return pct:sub(2)..tok
	else
	  return tokens[tok]
	end
      else
	-- No token, return unchanged.
	return pct..tok
      end
    end)
end

--- replace pattern if matched
--- @param fb function|string
--- @param line_start string
--- @param line_end string
--- @param s any
--- @return boolean|string
local function replace_pat( fb, line_start, line_end, s )
  --vim.print( 'replace:', pat, rep )

  if s.cn ~= nil  then
    -- Check if we are in the right treesitter capture group
    local cn = type(s.cn)=='string' and {s.cn} or s.cn
    local matched = false
    local captures = vim.treesitter.get_captures_at_cursor()
    if #captures == 0 and line_end:match( '^%s*$' ) then
      -- At the end of line, use captures from the previous non-blank instead
      local e = line_start:match( '%S%s*$' )
      if e then
	local pos = vim.api.nvim_win_get_cursor(0)
	pos[1] = pos[1] - 1

	captures = vim.tbl_map(
	  function (v) return v.capture end,
	  vim.treesitter.get_captures_at_pos( 0, pos[1], pos[2]-#e )
	)
      end
      -- vim.print( captures )
      -- vim.print( vim.treesitter.get_node_text( vim.treesitter.get_node( pos ), 0 ) )
    end
    for _, n in ipairs(cn) do
      if vim.startswith( n, '!' ) then
      	-- Match if n is not in captures
	if not vim.list_contains( captures, n:sub(2) ) then
	  matched = true
	end
      elseif vim.list_contains( captures, n ) then
	matched = true
      end
      if matched then break end
    end
    if not matched then
      return false -- Not in right treesitter capture; exit
    end
  end

  local mb = { line_start:match( s.mb .. '$' ) }
  if #mb > 0 and ( s.ma == nil or line_end:match( '^'..s.ma ) ) then
    local ret = ''
    if type( s.bs ) == 'number' then
      ret = ('<BS>'):rep(s.bs)
    elseif type( s.bs ) == 'string' and s.bs:match( '^%%%d$' ) then
      ret = ('<BS>'):rep( #mb[tonumber( s:sub(2) ) or 0] )
    end
    if type(s.rb) == 'string' then
      ret = fb .. ret .. replace_tokens( s.rb, mb )
    else
      ret = ret..s.rb( fb, line_start, mb )
    end

    if s.ra ~= nil then
      ret = ret..s.ra..('<Left>'):rep(#s.ra)
    end
    return ret
  end

  return false -- no match
end

local function replace( lhs, rhs, old_map )
  local pos = api.nvim_win_get_cursor(0)
  local cur_line = pos[1]-1
  local cur_col = pos[2]
  local line = api.nvim_buf_get_lines( 0, cur_line, cur_line+1, true )[1]
  local line_start = line:sub( 1, cur_col )
  local line_end = line:sub( cur_col+1 )

  local fb = fallback( lhs, old_map )
  local ret = fb
  if vim.endswith( fb, lhs ) then
    fb = fb:sub( 1, #fb - #lhs )
  end
  line_start = line_start .. fb
  --print( fb )

  if not M.disable then
    for n = #rhs, 1, -1 do
      -- Try entries in RHS in reverse order so more recently added entries
      -- have higher priority
      local spec = rhs[ n ]
      local r = replace_pat( fb, line_start, line_end, spec )
      if r ~= false then
	return r
      end
    end
  end

  -- No match; return unmodified fallback
  return ret
end

function M.imap( lhs, rhs )
  -- Only add imap if the filetype matches
  local r = vim.tbl_filter(
      function (v) return vim.list_contains( v.ft, vim.bo.ft ) end,
      rhs )
  if vim.tbl_isempty(r) then return end

  local old_map = vim.fn.maparg( lhs, 'i', false, true )
  if old_map.desc == 'imap' then old_map = {} end
  vim.keymap.set( 'i', lhs,
    function() return replace( lhs, r, old_map ) end,
    { noremap=true, expr=true, replace_keycodes=true, buffer=true, desc='imap' }
  )
end

local function tbl_split( str )
  local t = {}
  for i = 1, #str do
    t[i] = str:sub(i,i)
  end
  return t
end

function M.add_imap( key, spec )
  spec = vim.tbl_extend( 'keep', spec, {mb='.*', rb='', ft={} } )
  if type( spec.ft ) == 'string' then spec.ft = {spec.ft} end

  if M.imaps[key] == nil then
    M.imaps[key] = {}
  end
  for k, v in pairs(spec) do
    if type(v) == 'string' then
      spec[k] = replace_tokens( v, {k=key} )
    end
  end

  for _, v in ipairs( M.imaps[key] ) do
    local equal = true
    for _, k in pairs( {'mb', 'ma', 'cn', 'ft'} ) do
      if v[k] ~= spec[k] then
	equal = false
	break
      end
    end
    if equal then
      print( 'fancy-imaps duplicate pattern: ',
	'match_before='..(spec.mb or 'nil'),
	'match_after='..(spec.ma or 'nil'),
	'key='..key )
    end
  end
  table.insert( M.imaps[key], spec)
end

--[[
  map_list = { spec, spec, ..., [defaults={defaults}] }

  'spec' can be:
    { key_spec, match_before, match_after, opts... }
    { key_spec, {pat_spec, pat_spec, ...}

  'key_spec' can be:
    table of keys { 'x', 'y', ... }
    string of keys 'xy...'

  'pat_spec' should be:
    {mb=match_before, ma=match_after, rb=rep_before, ra=rep_after, cn=capture_names}

  cn is a list of treesitter capture names; map is only performed in one of these groups.
  (cn can also be a string if there is only one name.)

  match_after / match_after are patterns with token replacement
  rep_before / rep_after are strings with token replacement
    %k is replaced with the key
    %1 .. %9 are replaced with the corresponding match group
    %% is replaced with a %

  If defaults is specified, then each spec is merged with defaults. Useful to provide
  default options that apply to all specs (e.g. ft, cn)
--]]
---@class map_list
---@field defaults pat_spec
---@field [integer] map
---
---@class pat_spec pat_spec
---
---  @field mb string Match before (pattern with token replacement)
---  @field ma string Match after (pattern with token replacement)
---  @field rb string Replace before
---  @field ra string Replace after

--- add a list of imaps
--- @param map_list map_list
function M.add( map_list )
  local defaults = map_list.defaults or {}
  for _, m in ipairs( map_list ) do
    if type(m[1]) == 'string' then
      m[1] = tbl_split(m[1])
    end
    if #m == 3 then
      -- key, match_before, match_after
      m[2] = {{ mb=m[2], rb=m[3] }}
    elseif not vim.islist( m[2] ) then
      m[2] = {m[2]}
    end
    for _, k in ipairs( m[1] ) do
      for _, spec in ipairs( m[2] ) do
	if #spec == 2 then
	  -- in form {match_before, rep_before, ...}
	  spec = vim.tbl_extend( 'keep', spec, { mb=spec[1], rb=spec[2] } )
	  spec[1] = nil
	  spec[2] = nil
	end
	spec = vim.tbl_extend( 'keep', spec, defaults )
	M.add_imap( k, spec )
      end
    end
  end
end

function M.init()
  for k, r in pairs( M.imaps ) do
    M.imap( k, r )
  end
end

api.nvim_create_user_command( 'FancyImapsDisable',
  function () M.disable=true end, {} )
api.nvim_create_user_command( 'FancyImapsEnable',
  function () M.disable=false end, {} )

return M
