local M = {}

local digraphs = {
  [ [[\'(%a)]] ]="<c-k>'%1",
  [ [[\=(%a)]] ]='<c-k>-%1',
  [ [[\^(%a)]] ]='<c-k>>%1',
  [ [[\~(%a)]] ]='<c-k>~%1',
  [ [[\`(%a)]] ]='<c-k>`%1',
  [ [[\"(%a)]] ]='<c-k>:%1',
  [ [[\G%s+(%a)]] ]='<c-k>"%1',
  [ [[\G{(%a)}]] ]='<c-k>"%1',
  [ [[\k%s+(%a)]] ]='<c-k>;%1',
  [ [[\k{(%a)}]] ]='<c-k>;%1',
  [ [[\u%s+(%a)]] ]='<c-k>(%1',
  [ [[\u{(%a)}]] ]='<c-k>(%1',
  [ [[\v%s+(%a)]] ]='<c-k><%1',
  [ [[\v{(%a)}]] ]='<c-k><%1',
  [ [[\([Oo])%s+]] ]='<c-k>/%1',
  [ [[\\.(%a)]] ]='<c-k>%1.',
  [ [[\i%s]] ]='<c-k>i.',
}

local function strip_accents(s)
  for p, _ in pairs(digraphs) do
    s = s:gsub( p, '%1' )
  end
  return s
end

local function to_digraph(s)
  for p, r in pairs(digraphs) do
    s = s:gsub( p, r )
  end
  return s
end

function M.add(accents)
  local fa=require('fancy-abbr')
  local prefix={ '^', '[%s({%[]' }

  for _, t in ipairs( accents ) do
    if type(t) == 'string' then
      t = {a=strip_accents(t), t=t, d=to_digraph(t) }
    end

    if t.a:match('[^%a\\]') then
      print( ('Warning: possibly unrecognized accent: a=%s, t=%s, d=%s')
	:format( t.a, t.t, t.d ) )
    end
    fa.add( {p=prefix, a=t.a, r=t.d, ft='!^tex$'} )
    fa.add( {p=prefix, a=t.a, r=t.t, ft={'tex'}} )
  end

  -- Reset hash buckets
  fa.abbr_buckets = {}
end

return M
